package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */




public class Lab2 {

    
    public static void main(String[] args) {
        isLeapYear(1982);

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int size1 = word1.length();
        int size2 = word2.length();
        int size3= word3.length();
        
        if (size1 == size3 && size2 == size3){
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);            
        }
        else if (size1 > size2 && size1 > size3){
            System.out.println(word1);
        }
        else if (size2 > size1 && size2 > size3){
            System.out.println(word2);
        }
        else if (size3 > size1 && size3 > size2){
            System.out.println(word3);
        }
        else if (size1 == size2){
            System.out.println(word1);
            System.out.println(word2);
        }
        else if (size2 == size3){
            System.out.println(word2);
            System.out.println(word3);
        }
        else if (size1 == size3){
            System.out.println(word1);
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        return (year%4 == 0 && year%100 != 0) || year%400 == 0;
        /* 
        if ((year%4 == 0 && year%100 != 0) || year%400 == 0){
            return true;
        }
        else{
            return false;
        }
        */
    
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num%2 ==0){
            return true;
        }
        else{
            return false;
        }
    }

}

package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs       
    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i=7;i<=n;i+=7){
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i+":");
            for (int j = i; j <= i*n; j+=i){
                System.out.print(" "+j);
            }
            System.out.println();
            }
        }


    public static int crossSum(int num) {

        String s=Integer.toString(num);
        int sum=0;

        for (int i=0; i < s.length(); i++){
            sum += Character.getNumericValue(s.charAt(i));
        }

        return sum;
    }
}

package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs


    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {

        //make a new empty list
        ArrayList<Integer> outputList = new ArrayList<>();
        
        //loop through elements (n) in the list, if n is not a 3 it gets added to outputList
        for (int n : list) {
            if (n != 3){
                outputList.add(n);
            }
        }
        //return outputList
        return outputList;
    }

    //changed List<Integer> to ArrayList<Integer> due to output error
    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        //make a new list
        ArrayList<Integer> outputList = new ArrayList<>();

        //going though the list, if it doesnt contain n, add n to outputList, then return
        for (int n : list) {
            if (!outputList.contains(n)){
                outputList.add(n);
            }
        }
        return outputList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {

        // for list a length, set b(n) values to a(j) values at index(i)
        for (int i = 0; i < a.size(); i++){
            int j = a.get(i);
            int n = b.get(i);
            a.set(i, j+n);
        }
    }
}
package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/1/
 */
public class Lab1 {
    
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        //inside main static I run tasks 1 and 2
        task2();
    }

    public static void task1() {
        //define a variable for my welcoming message
        String theMessage =
        "Hei, det er meg, datamaskinen. \n" +
        "Hyggelig å se deg her.\n" +
        "Lykke til med INF101!\n";
        //print said message
        System.out.println(theMessage);
    }

    public static void task2() {
        //open the scanner
        sc = new Scanner(System.in); // Do not remove this line

        //ask user their info and store into variables(strings), using readInput class defined under
        String userAnswer1 = readInput("Hva er ditt navn?");
        String userAnswer2 = readInput("Hva er din adresse?");
        String userAnswer3 = readInput("Hva er ditt postnummer og poststed?");

        //print the results
        System.out.println(userAnswer1+"s adresse er:");
        System.out.println("");
        System.out.println(userAnswer1);
        System.out.println(userAnswer2);
        System.out.println(userAnswer3);
        

    }

        //readInput static down here

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */

    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}

package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        //for lenght of grid, if index i is row to be removed, remove it from grid
        for (int i = 0; i < grid.size(); i++){
            if (i == row){
                grid.remove(grid.get(i));
            }
        }
    }


    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        //init integers and arraylists to sum colums and rows

        int sumRows = 0;
        int sumCols = 0;
        ArrayList<Integer> checkSumsRows = new ArrayList<>();
        ArrayList<Integer> checkSumsCols = new ArrayList<>();

        //for each row and each column, sum the elements
        for (int row = 0; row < grid.size();row++){
            for (int ele = 0; ele < grid.size();ele++){
                sumRows += grid.get(row).get(ele);
                sumCols += grid.get(ele).get(row);
            }
        //store the sum values and reset the sums
        checkSumsRows.add(sumRows);
        checkSumsCols.add(sumCols);
        sumRows = 0;
        sumCols = 0;
        }

        //ctrl
        //System.out.println("SUMROWS-"+checkSumsRows);
        //System.out.println("SUMCOLS-"+checkSumsCols);
        
        //check that both rows and columns lists only contain the same integers, return false or true
        for (int i = 0; i < checkSumsRows.size()-1;i++){
            if (checkSumsRows.get(i) != checkSumsRows.get(i+1)){
                return false;
            }
        }
        for (int j = 0; j < checkSumsCols.size()-1;j++){
            if (checkSumsCols.get(j) != checkSumsCols.get(j+1)){
                return false;
            }
        }
        return true;
        }
}



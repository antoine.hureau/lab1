package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
    public static void main(String[] args) {
	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
     */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String errorMessage = "I do not understand cardboard. Could you try again?";
    String tieMessage = " It's a tie!";
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        System.out.println("Let's play round 1");
        int awnserHuman = awnserToInt();
        int awnserComputer = randNumb();
        if (awnserHuman != 0){
        }
        
        
        System.out.println("human "+awnserHuman);
        System.out.println("computer"+awnserComputer);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */

    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public int awnserToInt(){ //return int (123) after asking for user input
        int outputInt = 0;
        String inputString = readInput("Your choice (Rock/Paper/Scissors)?");
        inputString = inputString.toLowerCase();

        if (inputString != rpsChoices.get(0) || inputString != rpsChoices.get(1) || inputString != rpsChoices.get(2)){
            outputInt = 0;
        }
        else if (inputString == rpsChoices.get(0)){
            outputInt = 1;
        }
        else if (inputString == rpsChoices.get(1)){
            outputInt = 2;
        }
        else if (inputString == rpsChoices.get(2)){
            outputInt = 3;
        }
        return outputInt;
    }


    public int randNumb(){ //return random int (123)
        Random rand = new Random();
        int randInt = rand.nextInt(3);
        randInt += 1;
        return randInt;
    }
}